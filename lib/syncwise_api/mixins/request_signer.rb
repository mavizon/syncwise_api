module SyncwiseApi
  module Mixins
    module RequestSigner

      def sign
        if !self.instance_variable_defined?(:@params)
          fail SyncwiseApi::Errors::MissingInstanceVariable.new(:@request, SyncwiseApi::Mixins::RequestSigner, self)
        elsif !self.instance_variable_defined?(:@header)
          fail SyncwiseApi::Errors::MissingInstanceVariable.new(:@header, SyncwiseApi::Mixins::RequestSigner, self)
        else

        end
      end

      private


    end
  end
end