# Currently not used
#module SyncwiseApi
#  module Mixins
#    module RequestBuilder
#
#      def extract_header_params
#        # first, we have to make sure that this is mixed into a class that has a @param instance variable
#        # and that has a superclass with a required_header_params method
#        if !self.instance_variable_defined?(:@params)
#          fail SyncwiseApi::Errors::MissingInstanceVariable.new(:@params, SyncwiseApi::Mixins::RequestBuilder, self)
#        elsif !self.superclass.respond_to?(:required_header_params)
#          fail SyncwiseApi::Errors::MissingMethod.new(:required_header_params, SyncwiseApi::Mixins::RequestBuilder, self)
#        else
#          # as crazy as this looks, all it does is pull out the required header params from the @params hash, which holds
#          # any dynamically supplied params, and add them to the @header hash, which holds the header params
#          @header = {}
#          self.superclass.required_header_params.each { |key| @header[key] = @params.delete(key) }
#        end
#      end
#
#    end
#  end
#end