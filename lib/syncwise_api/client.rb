module SyncwiseApi
  class Client

    def initialize(username, password)
      @username = username
      @password = password
      login
    end

    def make_request(action_code, params)
      if action_code == :user_login
        puts 'Login is done automatically when a new Client is created. Ignoring this call.'
      else
        action_code = action_code.camelize
        if SyncwiseApi::Requests::V1_0.const_defined?(action_code)
          params[:'Action Code'] = action_code
          params[:'API Key'] = @username
          params[:secretKey] = @secret_key
          params[:accountId] = @account_id
          request_class = SyncwiseApi::Requests::V1_0.const_get(action_code)
          request = request_class.new(params)
          response = request.send_request
        else
          puts 'Sorry, that\'s not a valid request type.'
        end
      end
    end

    private

    def login
      begin
        request = SyncwiseApi::Requests::V1_0::UserLogin.new({:'API Key' => @username, :password => @password, :secretKey => '', :accountId => ''})
        response = request.send_request
        if response.valid?
          @secret_key = response.body_hash[:secret_key]
          @account_id = response.body_hash[:account_id]
        else
          fail SyncwiseApi::Errors::ApiErrorCode.new(response, request, response.header_hash, response.body_hash)
        end
      rescue => e
        SyncwiseApi::LOGGER.error e
      end

    end
  end
end