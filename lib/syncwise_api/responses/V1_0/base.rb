module SyncwiseApi
  module Responses
    module V1_0
      class Base

        def initialize(http_response_object)
          @http_response_object = http_response_object
          @body = @http_response_object.body
          @header_hash = @http_response_object.header.to_hash.symbolize_keys
          @body_hash = parse
        end

        attr_reader :body_hash

        def valid?
          @body_hash[:sts] == '1'
        end

        private

        def parse
          # if the response has a body, and the body is json, parse and store it in @body_hash; otherwise throw an error
          if @body.blank?
            fail SyncwiseApi::Errors::EmptyResponseBody.new(@http_response_object, @http_response_object.uri, @header_hash)
          elsif !@header_hash[:content_type].include?('application/json')
            fail SyncwiseApi::Errors::InvalidContentType.new(@http_response_object, @http_response_object.uri, @header_hash)
          else
            SyncwiseApi::ServiceUtils::Parsers::JSON.parse(@body).symbolize_keys
          end
        end

      end
    end
  end
end