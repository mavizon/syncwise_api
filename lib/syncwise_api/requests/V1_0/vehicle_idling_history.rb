module SyncwiseApi
  module Requests
    module V1_0
      class VehicleIdlingHistory < Base

        #TODO: this is janky. Fix it. Class constants? Something in Base? Not sure...
        my_required_params = [:deviceId, :endDate, :startDate]
        @required_params = my_required_params.concat(self.superclass.required_params)
        @required_params.sort!.freeze
        @verb = 'POST'

        class << self
          attr_reader :required_params, :verb
        end

      end
    end
  end
end