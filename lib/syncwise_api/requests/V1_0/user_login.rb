module SyncwiseApi
  module Requests
    module V1_0
      class UserLogin < Base

        #TODO: this is janky. Fix it. Class constants? Something in Base? Not sure...
        my_required_params = [:password]
        @required_params = my_required_params.concat(self.superclass.required_params)
        @required_params.sort!.freeze
        @verb = 'POST'

        class << self
          attr_reader :required_params, :verb
        end

        # overrides create_signature in Base, because for this request only, no signature is possible since we
        # don't have a secret key yet. the service still requires a dummy value here, though, so just quickly return a
        # valid, but dummy, signature string
        def create_signature
          'k8NipGDhzthk0EJRmHCBy2NcwSsC'
        end

      end
    end
  end
end