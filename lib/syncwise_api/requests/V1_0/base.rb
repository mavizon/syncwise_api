require 'openssl'

module SyncwiseApi
  module Requests
    module V1_0
      class Base
        #include SyncwiseApi::Mixins::RequestBuilder
        #include SyncwiseApi::Mixins::RequestSigner
        #include SyncwiseApi::Mixins::RequestSender

        # @required_header_params represents the keys required by all required for all Syncwise API calls, and CHANGE on a per call basis
        # thus, they must be present (have values) in the paramss passed to #initialize when creating a new object that
        # inherits from this class (Base)
        #TODO: Make this a class constant?
        @required_header_params = [:'API Key', :'Action Code']
        @required_header_params.freeze

        # @default_header_params holds key/values that are universally required for all Syncwise API calls, and DO NOT CHANGE
        #TODO: Make this a class constant?
        @default_header_params = {:'API Version' => 1.0, :'Signature Version' => 1, :'Signature Method' => 'HmacSHA256', :'Response Format' => 'JSON'}
        @default_header_params.freeze

        # @required_params represents all the params (both header and payload) that must be passed to #initialize when
        # creating a new object that inherits from this class (Base)
        #TODO: Make this a class constant?
        @required_params = [:secretKey, :accountId]
        @required_params.concat(@required_header_params)
        @required_params.freeze

        HOST_HEADER = 'api-int.syncwise.com'
        BASE_URL = "https://#{HOST_HEADER}/rest/action"

        class << self
          attr_reader :required_header_params, :default_header_params, :required_params, :host_header
        end

        def initialize(params = {})
          # Add the action code to params based on the child class's name
          params[:'Action Code'] = self.unqualified_class

          # Verify we have all required params by comparing child class's required_params to the passed in params
          param_keys = params.keys.sort

          fail SyncwiseApi::Errors::InvalidParameters.new(self.class, param_keys, self.class.required_params) unless param_keys & self.class.required_params == self.class.required_params
          # remove the signing key from params and store it for use when building the HMAC signature
          @secret_key = params.delete(:secretKey)

          # @params now holds the params we need to build the header_hash
          @params = params

          build_header_hash

          # after build_header_hash returns, we have @header which holds all values needed to build the request and signature
          # and @params which now only contains the values that will be added to the JSON payload
        end

        def send_request
          request_string = build_api_call
          body = build_body
          # TODO: send POST with Net::Http to request_string with JSON payload body
          SyncwiseApi::ServiceUtils::HTTP.post(request_string, body) do |http_response|
            SyncwiseApi::Responses::V1_0::Standard.new(http_response)
          end
        end

        private

        def build_header_hash
          # pull out the required header params from the @params hash, which holds
          # any dynamically supplied params, and add them to the @header hash, which holds the header params
          @header = {}.merge(SyncwiseApi::Requests::V1_0::Base.default_header_params)
          SyncwiseApi::Requests::V1_0::Base.required_header_params.each { |key| @header[key] = @params.delete(key) }
          @header[:Timestamp] = SyncwiseApi::ServiceUtils::Timestamper.stamp
        end

        def build_api_call
          "#{SyncwiseApi::Requests::V1_0::Base::BASE_URL}/#{@header[:'Action Code']}/#{@header[:'API Key']}/#{@header[:'API Version']}/#{@header[:'Signature Version']}/#{@header[:'Signature Method']}/#{create_signature}/#{@header[:Timestamp]}/#{@header[:'Response Format']}"
        end

        def build_body
          SyncwiseApi::ServiceUtils::Encoders::JSON.encode(@params)
        end

        def create_signature
          bin_hmac = SyncwiseApi::ServiceUtils::Crypto::HmacSha256.crypt(@secret_key, string_to_sign)
          base64_hmac = SyncwiseApi::ServiceUtils::Encoders::Base64.encode(bin_hmac)
          replace_bad_chars(base64_hmac)
        end

        def string_to_sign
          "#{self.class.verb}\n#{SyncwiseApi::Requests::V1_0::Base::HOST_HEADER}\n#{canonicalized_query_string}"
        end

        def canonicalized_query_string
          sorted_header_array = @header.to_a.sort
          query_string = ''
          sorted_header_array.each do |key_value_array|
            query_string = query_string + "&#{key_value_array[0]}=#{key_value_array[1]}"
          end

          query_string
        end

        def replace_bad_chars(encoded_string)
          encoded_string.gsub!('/', 'A')
          encoded_string.gsub!('+', 'B')
          encoded_string.gsub!('=', 'C')
          encoded_string.gsub!('&', 'D')
          encoded_string
        end

      end
    end
  end
end