module SyncwiseApi
  module Errors

    class SyncwiseError < StandardError

    end

    class MissingInstanceVariable < SyncwiseError
      def initialize(variable, mixin, receiver)
        @variable = variable
        @mixin = mixin
        @receiver_inspect = receiver.inspect
        super("Mixin #{@mixin} tried to use undefined instance variable #{@variable} in #{@receiver_inspect}")
      end

      attr_reader :variable, :mixin, :receiver_inspect
    end

    class InvalidParameters < SyncwiseError
      def initialize(request_type, passed_params, required_params)
        @request_type = request_type
        @passed_params = passed_params
        @required_params = required_params
        super("#{@request_type} was called with params #{@passed_params}, but requires params #{@required_params}")
      end

      attr_reader :request_type, :passed_params, :required_params
    end

    class JSONParseError < SyncwiseError
      def initialize(error)
        @error = error
        super("Error parsing JSON: #{@error}")
      end

      attr_reader :error
    end

    class JSONEncodeError < SyncwiseError
      def initialize(object)
        @object = object
        super("Error encoding object into JSON. Object: #{@object.inspect}")
      end

      attr_reader :object
    end

    class HttpError < SyncwiseError
      def initialize(error, request)
        @error = error
        @request = request
        super("Http Error when trying to make request. Object: #{@error.inspect}, request: #{@request}")
      end

      attr_reader :error, :request
    end

    class SyncwiseApi::Errors::EmptyResponseBody < SyncwiseError
      def initialize(resp, request, header)
        @resp = resp
        @request = request
        @header = header
        super("Api call returned with empty body. Response: #{@resp}, request: #{@request}, header: #{@header}")
      end

      attr_reader :resp, :request, :header
    end

    class SyncwiseApi::Errors::InvalidContentType < SyncwiseError
      def initialize(resp, request, header)
        @resp = resp
        @request = request
        @header = header
        super("Api call returned an invalid Content-Type. Response: #{@resp}, request: #{@request}, header: #{@header}")
      end

      attr_reader :resp, :request, :header
    end

    class SyncwiseApi::Errors::ApiErrorCode < SyncwiseError
      def initialize(resp, request, header, body)
        @resp = resp
        @request = request
        @header = header
        @body = body
        super("Api call returned a Syncwise Error Code. Response: #{@resp}, request: #{@request}, header: #{@header}, body: #{@body}")
      end

      attr_reader :resp, :request, :header, :body
    end

  end
end