require 'yajl'

module SyncwiseApi
  module ServiceUtils
    module Encoders
      module JSON

        def encode(object)
          if object.is_a?(Hash) || object.is_a?(Array)
            Yajl::Encoder.encode(object)
          else
            fail SyncwiseApi::Errors::JSONEncodeError.new(object)
          end
        end

        module_function :encode

      end
    end
  end
end