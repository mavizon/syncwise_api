require 'yajl'

module SyncwiseApi
  module ServiceUtils
    module Parsers
      module JSON

        def parse(json_string)

          recursive_parse = true
          parsed = ''

          if !json_string.blank? && json_string.is_a?(String)
            begin
              parsed = Yajl::Parser.parse(json_string)
              parsed = parsed.symbolize_keys if parsed.is_a?(Hash)
            rescue => e
              SyncwiseApi::Errors::JSONParseError.new(e)
            end
          end

          while recursive_parse
            recursive_parse = sub_parse!(parsed)
          end

          parsed
        end

        module_function :parse

        private

        def self.sub_parse!(hash)
          had_to_sub_parse = false
          if hash.is_a?(Hash)
            hash.each do |k, v|
              if json_as_string?(v)
                had_to_sub_parse = true
                hash[k] = parse(v)
              end
            end
          end

          had_to_sub_parse
        end

        def self.json_as_string?(value)
          if value.is_a?(String)
            value.include?('{') || value.include?('[')
          end
        end
      end
    end
  end
end