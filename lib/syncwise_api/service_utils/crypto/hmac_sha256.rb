module SyncwiseApi
  module ServiceUtils
    module Crypto
      module HmacSha256

        def crypt(secret_key, string_to_sign)
          digest = OpenSSL::Digest::Digest.new('sha256')
          OpenSSL::HMAC.digest(digest, secret_key, string_to_sign)
        end

        module_function :crypt
      end
    end
  end
end