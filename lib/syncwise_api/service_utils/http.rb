require 'net/http'

module SyncwiseApi
  module ServiceUtils
    module HTTP

      JSON_MIME = 'application/json'

      # callback will receive the response body
      def post(request_url_string, body, &callback)
        uri = URI(request_url_string)
        req = Net::HTTP::Post.new(uri)
        req.body = body
        req.content_type = JSON_MIME
        req['Accept'] = JSON_MIME
        Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) do |http|
          begin
            resp = http.request(req)
            # throws HTTP error if response.class is not Net::HTTPSuccess
            resp.value
            callback.call(resp)
          rescue SyncwiseApi::Errors::SyncwiseError => e
            SyncwiseApi::LOGGER.error e
          rescue => e
            fail SyncwiseApi::Errors::HttpError.new(e, request_url_string)
          end
        end
      end

      module_function :post

    end
  end
end