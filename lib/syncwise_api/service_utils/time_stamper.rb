module SyncwiseApi
  module ServiceUtils
    module Timestamper
      FORMAT_STRING = '%y%m%d%H%M%S%z'

      def stamp
        Time.now.utc.strftime(FORMAT_STRING)
      end

      module_function :stamp
    end
  end
end