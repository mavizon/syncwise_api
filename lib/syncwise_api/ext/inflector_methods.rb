require_relative 'inflections'

# holds 'inflector methods' (to use a Rails term) to change 'words' from one form to another
module SyncwiseApi
  module Inflector

    # downcase words, and replace ' ' with '_'. Example 'Big Bob'.dehumanize => 'big_bob'
    def dehumanize(word)
      # should be OK to remove the respond_to? as long as we ensure we call this only on Strings or things that can be
      # cast to Strings
      #if word.respond_to?(:to_s)
      dh_word = word.to_s.dup
      dh_word.gsub!(/ /, '_')
      dh_word.downcase!
      dh_word
      #end
    end

    def underscore(camel_cased_word)
      # should be OK to remove the respond_to? as long as we ensure we call this only on Strings or things that can be
      # cast to Strings
      #if camel_cased_word.respond_to?(:to_s)
      word = camel_cased_word.to_s.dup
      word.gsub!(/::/, '/')
      word.gsub!(/(?:([A-Za-z\d])|^)(#{inflections.acronym_regex})(?=\b|[^a-z])/) { "#{$1}#{$1 && '_'}#{$2.downcase}" }
      word.gsub!(/([A-Z\d]+)([A-Z][a-z])/,'\1_\2')
      word.gsub!(/([a-z\d])([A-Z])/,'\1_\2')
      word.tr!("-", "_")
      word.downcase!
      word
      #end
    end

    # By default, +camelize+ converts strings to UpperCamelCase. If the argument to +camelize+
    # is set to <tt>:lower</tt> then +camelize+ produces lowerCamelCase.
    #
    # +camelize+ will also convert '/' to '::' which is useful for converting paths to namespaces.
    #
    # Examples:
    #   "active_model".camelize                # => "ActiveModel"
    #   "active_model".camelize(:lower)        # => "activeModel"
    #   "active_model/errors".camelize         # => "ActiveModel::Errors"
    #   "active_model/errors".camelize(:lower) # => "activeModel::Errors"
    #
    # As a rule of thumb you can think of +camelize+ as the inverse of +underscore+,
    # though there are cases where that does not hold:
    #
    #   "SSLError".underscore.camelize # => "SslError"
    def camelize(term, uppercase_first_letter = true)
      string = term.to_s
      if uppercase_first_letter
        string = string.sub(/^[a-z\d]*/) { inflections.acronyms[$&] || $&.capitalize }
      else
        string = string.sub(/^(?:#{inflections.acronym_regex}(?=\b|[A-Z_])|\w)/) { $&.downcase }
      end
      string.gsub(/(?:_|(\/))([a-z\d]*)/i) { "#{$1}#{inflections.acronyms[$2] || $2.capitalize}" }.gsub('/', '::')
    end

    module_function :dehumanize, :underscore, :camelize

  end
end