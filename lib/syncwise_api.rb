require 'logger'
require 'connection_pool'
require_relative 'syncwise_api/version'
require_relative 'syncwise_api/errors'
require_relative 'syncwise_api/service_utils/service_utils'
require_relative 'syncwise_api/ext/core_ext'
require_relative 'syncwise_api/mixins/mixins'
require_relative 'syncwise_api/requests/requests'
require_relative 'syncwise_api/responses/responses'
require_relative 'syncwise_api/client'

module SyncwiseApi
  LOGGER = Logger.new(STDERR)

  def start(username, password, number_of_clients = 1)
    @clients ||= ConnectionPool.new(size: number_of_clients, timeout: 5) { SyncwiseApi::Client.new(username, password)}
  end

  def make_request(action_code, params)
    if @clients
      @clients.with do |client|
        client.make_request(action_code, params)
      end
    else
      puts 'You must first call SyncwiseAPI.start(username, password) before you can call SyncwiseApi.make_request.'
    end
  end

  module_function :start, :make_request
end
