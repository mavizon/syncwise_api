# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'syncwise_api/version'

Gem::Specification.new do |spec|
  spec.name          = "syncwise_api"
  spec.version       = SyncwiseApi::VERSION
  spec.authors       = ["Brendten Eickstaedt"]
  spec.email         = ["brendten@brendteneickstaedt.com"]
  spec.description   = %q{Syncwise API library for Ruby}
  spec.summary       = %q{Syncwise API library for Ruby}
  spec.homepage      = "https://bitbucket.org/mavizon/syncwise_api"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency 'yajl-ruby', '~> 1.1.0'
  spec.add_dependency 'connection_pool', '~> 1.0.0'

  spec.add_development_dependency 'bundler', '~> 1.3'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'pry'
end
