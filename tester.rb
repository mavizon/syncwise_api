#!/usr/bin/env ruby

require 'syncwise_api'
require 'pry'

begin
  binding.pry
rescue Exception => e
  SyncwiseApi::LOGGER.error e
end
